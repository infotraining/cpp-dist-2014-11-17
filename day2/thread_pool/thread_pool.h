#ifndef THREAD_POOL_H
#define THREAD_POOL_H
#include <functional>
#include <vector>
#include <thread>
#include "../TSQ/thread_safe_queue.h"
#include <future>

using Task = std::function<void()>;

class thread_pool
{
    thread_safe_queue<Task> q;
    std::vector<std::thread> workers;

public:
    thread_pool(int pool_size)
    {
        for (int i = 0 ; i < pool_size ; ++i)
            workers.emplace_back([this]
            {
                for(;;)
                {
                    Task task;
                    q.pop(task);
                    if (!task) return;
                    task();
                }
            });
    }

    template<typename F>
    auto async(F fun) -> std::future<decltype(fun())>
    {
        using Res = decltype(fun());
        auto task = std::make_shared<std::packaged_task<Res()>>(fun);
        auto res = task->get_future();
        q.push([task] { (*task)(); });
        return res;
    }

    void submit(Task t)
    {
        if (t)
            q.push(t);
    }
    ~thread_pool()
    {
        for (auto& th : workers) q.push(nullptr);
        for (auto& th : workers) th.join();
    }
};

#endif // THREAD_POOL_H

#include <iostream>
#include "thread_pool.h"


using namespace std;

void hello()
{
    cout << "Hello" << endl;
}

int answer()
{
    return 42;
}

int main()
{
    thread_pool tp(2);
    tp.submit(hello);
    tp.submit([] { cout << "Hello from lambda" << endl; });
    future<int> res = tp.async(answer);
    cout << "Result = "  << res.get() << endl;
    for (int i = 1 ; i < 20 ; ++i)
    {
        tp.submit([i]{
            this_thread::sleep_for(chrono::seconds(1));
            cout << "message from " << i << " lambda" << endl;
        });
    }
    cout << "Hello World!" << endl;
    return 0;
}


#include <iostream>
#include <queue>
#include <thread>
#include <condition_variable>
#include <mutex>
#include <vector>

using namespace std;

queue<int> q;
mutex mtx;
condition_variable cond;

void producer()
{
    for (int i = 0 ; i < 20 ; ++i)
    {
         {
            lock_guard<mutex> lg(mtx);
            q.push(i);
            cout << "produced " << i << endl;
            cond.notify_one();
         }
         this_thread::sleep_for(chrono::milliseconds(200));
    }
}

void consumer(int id)
{
    for(;;)
    {
        unique_lock<mutex> lg(mtx);
        cond.wait(lg, [] { return !q.empty(); });
        cout << "cons " << id << " just got " << q.front() << endl;
        q.pop();
    }
}

int main()
{
    vector<thread> thds;
    thds.emplace_back(producer);
    thds.emplace_back(consumer, 1);
    thds.emplace_back(consumer, 2);
    thds.emplace_back(consumer, 3);
    thds.emplace_back(consumer, 4);
    for(thread& th : thds) th.join();
    return 0;
}



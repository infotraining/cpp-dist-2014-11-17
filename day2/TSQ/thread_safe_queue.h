#ifndef THREAD_SAFE_QUEUE_H
#define THREAD_SAFE_QUEUE_H
#include <queue>
#include <condition_variable>
#include <mutex>

template<typename T>
class thread_safe_queue
{
    std::mutex mtx;
    std::condition_variable cond;
    std::queue<T> q;
public:
    thread_safe_queue() {}

    void pop(T& item)
    {
        std::unique_lock<std::mutex> lg(mtx);
        cond.wait(lg, [this] { return !q.empty(); });
        item = std::move(q.front());
        q.pop();
    }

    void push(const T& item)
    {
        std::lock_guard<std::mutex> lg(mtx);
        q.push(item);
        cond.notify_one();
    }

    void push(T&& item)
    {
        std::lock_guard<std::mutex> lg(mtx);
        q.push(std::move(item));
        cond.notify_one();
    }
};

#endif // THREAD_SAFE_QUEUE_H

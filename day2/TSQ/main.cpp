#include <iostream>
#include "thread_safe_queue.h"
#include <vector>
#include <thread>

using namespace std;

thread_safe_queue<int> q;

void producer()
{
    for(int i = 0 ; i < 100 ; ++i)
    {
        q.push(i);
    }
}

void consumer(int id)
{
    for(;;)
    {
        int item;
        q.pop(item);
        cout << "I, " << id << " just got " << item << endl;
    }
}

int main()
{
    vector<thread> thds;
    thds.emplace_back(producer);
    thds.emplace_back(consumer, 1);
    thds.emplace_back(consumer, 2);
    thds.emplace_back(consumer, 3);
    thds.emplace_back(consumer, 4);
    for(thread& th : thds) th.join();
    return 0;
}


#include <iostream>
#include <thread>
#include <future>
#include <stdexcept>

using namespace std;

int answer(int id)
{
    this_thread::sleep_for(chrono::seconds(2));
    if (id == 13)
        throw std::logic_error("13 is not valid");
    return 42;
}

int main()
{
    //future<int> result = async(launch::async, answer, 13);
    packaged_task<int(int)> ptask(answer);
    future<int> result = ptask.get_future();
    thread th(move(ptask), 13);
    th.detach();
    cout << "After launching" << endl;
    return 0;
    result.wait();
    try
    {
        cout << "Result = " << result.get() << endl;
    }
    catch(...)
    {
        cout << "got exception..." << endl;
    }
    return 0;
}


#include <iostream>
#include <thread>
#include <vector>
#include <algorithm>
#include <atomic>
#include <condition_variable>
#include <mutex>

using namespace std;

namespace atom
{
class Data
{
    vector<int> data_;
    //volatile bool is_ready; // works... sort of
    atomic<bool> is_ready;

public:
    Data() : is_ready(false) {}

    void read()
    {
        cout << "Reading data" << endl;
        this_thread::sleep_for(chrono::seconds(100));
        data_.resize(100);
        generate(data_.begin(), data_.end(), [] {return rand() % 100 ;});
        is_ready = true;
        cout << "data is ready" << endl;
    }

    void process()
    {
        for(;;)
        {
            if (is_ready)
            {
                cout << "processing ..." << endl;
                long sum = accumulate(data_.begin(), data_.end(), 0L);
                cout << "sum = " << sum << endl;
                return;
            }
            this_thread::sleep_for(chrono::microseconds(10));
        }
    }
};

}

namespace cond
{
class Data
{
    vector<int> data_;
    mutex mtx;
    condition_variable cond;
    bool is_ready;

public:
    Data() : is_ready(false) {}

    void read()
    {
        cout << "Reading data" << endl;
        this_thread::sleep_for(chrono::seconds(100));
        data_.resize(100);
        generate(data_.begin(), data_.end(), [] {return rand() % 100 ;});
        {
            lock_guard<mutex> lg(mtx);
            is_ready = true;
        }
        cond.notify_one();
        cout << "data is ready" << endl;
    }

    void process()
    {
        unique_lock<mutex> lk(mtx);
        //while(!is_ready)
        //    cond.wait(lk);
        cond.wait(lk, [this] { return is_ready;});
        cout << "processing ..." << endl;
        long sum = accumulate(data_.begin(), data_.end(), 0L);
        cout << "sum = " << sum << endl;
        return;
    }
};
}

using namespace cond;

int main()
{
    Data data;
    thread th1( [&data] {data.read();});
    thread th2( [&data] {data.process();});
    th1.join();
    th2.join();
    return 0;
}


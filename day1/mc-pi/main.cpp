#include <iostream>
#include <random>
#include <thread>
#include <vector>
#include <algorithm>
#include <chrono>
#include <mutex>

using namespace std;
mutex mtx;

void calc_pi(const unsigned long N, unsigned long& counter)
{
    random_device rd;
    mt19937_64 gen(rd());
    uniform_real_distribution<> dis(0,1);
    unsigned long tmp = 0;
    double x, y;
    for (long i = 0 ; i < N ; ++i)
    {
        x = dis(gen);
        y = dis(gen);
        if( x*x + y*y < 1)
        {
            ++tmp;
        }
    }
    mtx.lock();
    counter += tmp;
    mtx.unlock();
}

int main()
{
    const unsigned long N = 100000000;
    unsigned long counter = 0;
    vector<thread> thds;

    int n_of_threads = max(thread::hardware_concurrency(), (unsigned)1);
    n_of_threads = 4;
    vector<unsigned long> counters(n_of_threads);

    auto start = chrono::high_resolution_clock::now();
    for (int i = 0 ; i < n_of_threads ; ++i)
    {
        thds.emplace_back(calc_pi, N/n_of_threads, ref(counter));//ref(counters[i]));
    }
    for(auto& th : thds) th.join();
    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed multithreading ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
    cout << "Pi = " << double(accumulate(counters.begin(), counters.end(), 0L))/double(N) * 4 << endl;

    // single threaded
    start = chrono::high_resolution_clock::now();
    calc_pi(N, counter);
    end = chrono::high_resolution_clock::now();
    cout << "Elapsed single threaded ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
    cout << "Pi = " << double(counter)/double(N) * 4 << endl;

    return 0;
}


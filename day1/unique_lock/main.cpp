#include <iostream>
#include <thread>
#include <mutex>

using namespace std;
timed_mutex mtx;

mutex nmtx;

unique_lock<mutex> prepare()
{
    unique_lock<mutex> lk(nmtx);
    cout << "Preparing data" << endl;
    //return lk;
}

void process()
{
    cout << "Started processing" << endl;
    //unique_lock<mutex> lk(prepare());
    //prepare();
    cout << "Working with data" << endl;
}

void worker()
{
    cout << "Worker has started " << this_thread::get_id() << endl;
    unique_lock<timed_mutex> lk(mtx, try_to_lock);
    if(lk.owns_lock()) {
        cout << "Worker has got mutex " << this_thread::get_id() << endl;
        this_thread::sleep_for(chrono::seconds(2));
    }
    else
    {
        do
        {
            cout << "Waiting for mutex " << this_thread::get_id() << endl;
        } while(!lk.try_lock_for(chrono::milliseconds(200)));
    }

    cout << "Worker finished " << this_thread::get_id() << endl;
}

int main()
{
    cout << "Hello World!" << endl;
    thread th1(worker);
    thread th2(worker);
    th1.join();
    th2.join();
    return 0;
}


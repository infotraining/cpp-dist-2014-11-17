#include <iostream>
#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void hello()
{
    cout << "Hello from thread" << endl;
}

void hello2(int id)
{
    cout << "Hello from " << id << endl;
}

void result(int& res)
{
    res = 42;
}

class Task
{
    int id_;
public:
    Task(int id) : id_(id) {}
    void operator()(int delay)
    {
        this_thread::sleep_for(chrono::milliseconds(delay));
        cout << "Task " << id_ << endl;
    }
};

thread generate_task()
{
    Task task(10);
    return thread(task, 100);
}

int main()
{
    vector<thread> thds;
    for (int i = 1 ; i < 11 ; ++i)
    {
//        thread tmp(hello2, i);
//        thds.push_back(move(tmp));
//        thds.push_back(thread(hello2, i));
        thds.emplace_back(hello2, i);
    }

    thds.push_back(generate_task());

    for (auto& th : thds)
        th.join();

    return 0;
}


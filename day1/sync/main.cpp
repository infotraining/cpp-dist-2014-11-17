#include <iostream>
#include <thread>
#include <vector>
#include <mutex>
#include <atomic>

using namespace std;

unsigned long counter = 0;
const unsigned long N = 1000000;
mutex mtx;
atomic<unsigned long> atomic_counter{0};

void increase()
{
    for (unsigned long i = 0 ; i < N ; ++i)
    {
        lock_guard<mutex> lg(mtx);
        //if (counter == 1000) return;
        ++counter;
    }
}

int main()
{
    cout << "Hello World!" << endl;
    vector<thread> thds;
    auto start = chrono::high_resolution_clock::now();
    for (int i = 0 ; i < 2 ; ++i)
    {
        thds.emplace_back(increase);
    }
    for(auto&th : thds) th.join();
    auto end = chrono::high_resolution_clock::now();
    cout << "Elapsed mutex ";
    cout << chrono::duration_cast<chrono::milliseconds>(end-start).count();
    cout << " ms" << endl;
    cout << "Counter = " << counter << endl;
    return 0;
}


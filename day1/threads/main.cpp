#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void hello()
{
    cout << "Hello from thread" << endl;
}

void hello2(int id)
{
    cout << "Hello from " << id << endl;
}

void result(int& res)
{
    res = 42;
}

class Task
{
    int id_;
public:
    Task(int id) : id_(id) {}
    void operator()(int delay)
    {
        this_thread::sleep_for(chrono::milliseconds(delay));
        cout << "Task " << id_ << endl;
    }
};

class Buff
{
    vector<int> buff;
public:
    void assign(const vector<int>& src)
    {
        buff.assign(src.begin(), src.end());
    }

    vector<int> data() const
    {
        return buff;
    }
};

int main()
{
    cout << "Hello World!" << endl;
    thread th1(&hello2, 10);
    int answer = 0;
    thread th2(result, ref(answer));
    th1.join();
    //cout << "Ans = " << answer << endl;
    th2.join();
    // functors
    Task task(100);
    thread th3(task, 1000);
    cout << "Ans = " << answer << endl;
    th3.join();
    // methods
    vector<int> vec = {1,2,3,4,5,6};
    Buff buff;
    //thread th4(&Buff::assign, &buff, cref(vec) );
    //thread th4(bind(&Buff::assign, &buff, cref(vec)) );
    thread th4( [&] { buff.assign(vec); } );
    th4.join();
    for (auto& el : buff.data())
    {
        cout << el << " ";
    }
    cout << endl;
    return 0;
}


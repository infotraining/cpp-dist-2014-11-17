#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <string>

using namespace std;
using namespace AmqpClient;

string ex_name = "Nokia-test";
//string q_name = "Nokia-q";

void receive()
{
    Channel::ptr_t channel;
    channel = Channel::Create("192.168.1.2",
                              5672,
                              "admin", "tymczasowe");

    string q_name = channel->DeclareQueue("");
    channel->BindQueue(q_name, ex_name);
    channel->BasicConsume(q_name);
    // receive message
    for (;;)
    {
        BasicMessage::ptr_t msg = channel->BasicConsumeMessage()->Message();
        cout << msg->Body() << endl;
    }
}

int main()
{
    cout << "Starting app" << endl;
    receive();
    return 0;
}



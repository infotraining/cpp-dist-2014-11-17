#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <string>

using namespace std;
using namespace AmqpClient;

string ex_name = "Nokia-test";

void send()
{
    Channel::ptr_t channel;
    channel = Channel::Create("192.168.1.2",
                              5672,
                              "admin", "tymczasowe");

    // sending message
    for (;;)
    {
        string msg;
        cout << ">>> ";
        std::getline(cin, msg);
        msg = "[Leszek] " + msg;
        channel->BasicPublish(ex_name, "chat",
                          BasicMessage::Create(msg));
    }
}

int main()
{
    cout << "Starting app" << endl;
    send();
    //receive();
    return 0;
}


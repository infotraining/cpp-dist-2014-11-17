#include <iostream>
#include <zhelpers.hpp>
#include <zmq.hpp>

using namespace std;

// CLIENT

int main()
{
    cout << "Client is starting" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_REQ); // REQUEST

    socket.connect("tcp://192.168.1.2:5555");

    for (int i = 0 ; i < 10 ; ++i)
    {
        s_send(socket, "hello my beloved server");
        cout << "answer = " << s_recv(socket) << endl;
    }

    return 0;
}


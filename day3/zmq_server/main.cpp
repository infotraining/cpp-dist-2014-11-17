#include <iostream>
#include <zhelpers.hpp>
#include <zmq.hpp>

using namespace std;

// SERVER

int main()
{    
    cout << "ZMQ server starting" << endl;
    zmq::context_t context(1);
    zmq::socket_t socket(context, ZMQ_REP); // REPLY

    socket.bind("tcp://*:5555");

    for(;;)
    {
        cout << "from client: " << s_recv(socket) << endl;
        s_send(socket,"msg from server");
    }
    return 0;
}


#include <iostream>
#include <SimpleAmqpClient/SimpleAmqpClient.h>
#include <string>

using namespace std;
using namespace AmqpClient;

string ex_name = "Nokia-test";
string q_name = "Nokia-q";

void setup()
{
    Channel::ptr_t channel;
    channel = Channel::Create("192.168.1.2",
                              5672,
                              "admin", "tymczasowe");

    // creating queue and exchange point
    channel->DeclareExchange(ex_name, Channel::EXCHANGE_TYPE_FANOUT );
    //channel->DeclareQueue(q_name, false, true, false, false);
    //channel->BindQueue(q_name, ex_name, "chat");
}

void send()
{
    Channel::ptr_t channel;
    channel = Channel::Create("192.168.1.2",
                              5672,
                              "admin", "tymczasowe");

    // sending message
    channel->BasicPublish(ex_name, "chat",
                          BasicMessage::Create("medium is the message"));
}

void receive()
{
    Channel::ptr_t channel;
    channel = Channel::Create("192.168.1.2",
                              5672,
                              "admin", "tymczasowe");
    channel->BasicConsume(q_name);
    while(true)
    {
        BasicMessage::ptr_t msg = channel->BasicConsumeMessage()->Message();
        cout << msg->Body() << endl;
    }
}

int main()
{
    cout << "Starting app" << endl;
    //send();
    //receive();
    setup();
    return 0;
}


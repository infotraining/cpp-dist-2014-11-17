#include <iostream>
#include <zhelpers.hpp>
#include <zmq.hpp>
#include <thread>
using namespace std;

// SERVER

int main()
{
    cout << "ZMQ renderer server starting" << endl;
    zmq::context_t context(1);
    //zmq::socket_t socket(context, ZMQ_PUSH); // PUSH
    zmq::socket_t socket(context, ZMQ_ROUTER); // PUSH

    socket.bind("tcp://192.168.1.2:8888");

    for(int line = 0 ; line < 768 ; ++line)
    {
        // handshake
        string addres = s_recv(socket);
        s_recv(socket);
        string id = s_recv(socket);
        s_sendmore(socket, addres);
        s_sendmore(socket, "");
        // sending line
        string msg = to_string(line);
        msg += " ";
        msg += to_string(64);
        s_send(socket, msg);
        cout << "line: " << msg << endl;
    }
    return 0;
}


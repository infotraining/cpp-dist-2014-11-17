## Multithreading and distributed programming in C++

### Additonal information

#### login and password for VM:

```
dev  /  tymczasowe
```

#### reinstall VBox addon

```
sudo /etc/init.d/vboxadd setup
```

#### proxy settings

We can add them to `.profile`

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```

#### GIT

```
git clone https://bitbucket.org/infotraining/cpp-dist-2014-11-17
```

[git cheat sheet](http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf)

#### Libs

```
git clone https://github.com/alanxz/rabbitmq-c
git clone https://github.com/alanxz/SimpleAmqpClient
```

Build rabbitmq-c first!

```
mkdir build && cd build
cmake ..
cmake --build .
cmake --build . --target install
```

### Linki

http://www.1024cores.net/

http://preshing.com/

http://herbsutter.com/

http://www.cs.wustl.edu/~schmidt/ACE.html

https://software.intel.com/en-us/intel-tbb

http://stackoverflow.com/questions/11227809/why-is-processing-a-sorted-array-faster-than-an-unsorted-array

http://blog.bfitz.us/?p=491 - fibers, cooperative scheduling

http://lwn.net/Articles/250967/ - What every programmer should know about memory